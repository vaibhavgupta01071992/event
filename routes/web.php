<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/event/list_event');
});
Route::get('/event/create', 'EventController@createEvent');
Route::get('/event/list', 'EventController@indexEvent');
Route::post('/event/store', 'EventController@storeEvent');