<!DOCTYPE html>
<html lang="en">
<head>
  <title>Add Event</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Add Event</h2>

@if($errors->any())
    {!! implode('', $errors->all('<div>:message</div>')) !!}
@endif
  <form action="{{ url('event/store/') }}" method="post">
    @csrf <!-- {{ csrf_field() }} -->
    <div class="form-group">
      <label for="title">Title:</label>
      <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
    </div>
    <div class="form-group">
      <label for="start_date">Start Date:</label>
      <input type="date" class="form-control datepicker" id="start_date" placeholder="Enter Start Date" name="start_date">
    </div>
    <div class="form-group">
      <label for="end_date">End Date:</label>
      <input type="date" class="form-control datepicker" id="end_date" placeholder="Enter End Date" name="end_date">
    </div>

    <div class="form-group">
      <label for="end_date">Recurrence:</label>
    </div>
    <div class="form-group">
      <input type="radio" name="recurrence"  value="repeat">Repeat 
      <select name="recurrence_every" class="recurrence_every">
      	<option value="">Select</option>
      	<option value="1">Every</option>
      	<option value="2">Every Other</option>
      	<option value="3">Every Third</option>
      	<option value="4">Every Fourth</option>
      </select>
      <select name="recurrence_day" class="recurrence_day">
      	<option value="">Select</option>
      	<option value="Day">Day</option>
      	<option value="Week">Week</option>
      	<option value="Month">Month</option>
      	<option value="Year">Year</option>
      </select>
    </div>
    <div class="form-group">
      <input type="radio" name="recurrence" value="repeat_on_the">Repeat on the 
      <select name="recurrence_First" class="recurrence_First">
      	<option value="">Select</option>
      	<option value="1">First</option>
      	<option value="2">Second</option>
      	<option value="3">Third</option>
      	<option value="4">Fourth</option>
      </select>
      <select name="recurrence_weekday" class="recurrence_weekday">
      	<option value="">Select</option>
      	<option value="Sunday">Sunday</option>
      	<option value="Monday">Monday</option>
      	<option value="Tuesday">Tuesday</option>
      	<option value="Wednesday">Wednesday</option>
      	<option value="Thursday">Thursday</option>
      	<option value="Friday">Friday</option>
      	<option value="Saturday">Saturday</option>
      </select>
      <select name="recurrence_month" class="recurrence_month">
      	<option value="">Select</option>
      	<option value="3">3 Months</option>
      	<option value="4">4 Months</option>
      	<option value="6">6 Months</option>
      	<option value="12">Year</option>
      </select>
    </div>


    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>
<script type="text/javascript">
	$('.datepicker').datepicker();
</script>
</body>
</html>
