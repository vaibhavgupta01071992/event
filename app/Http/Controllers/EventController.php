<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\Recurrences;
use Redirect;
class EventController extends Controller
{
   public function createEvent() {
      return \View::make("event/add_event");
   }
   public function indexEvent() {
      $Events=Events::all();
      print_r($Events->recurrence);
     die('dsfs');

      return \View::make("event/add_event");
   }
   public function storeEvent(Request $request) {
         $rules = array(
            'title'=>'required|max:8',
            'start_date'=>'required',
            'end_date'=>'required',
            'recurrence' => 'required|in:repeat_on_the,repeat',
            'recurrence_First' => 'sometimes|nullable|required_if:recurrence,repeat_on_the',
            'recurrence_weekday' => 'sometimes|nullable|required_if:recurrence,repeat_on_the',
            'recurrence_month' => 'sometimes|nullable|required_if:recurrence,repeat_on_the',
            'recurrence_every' => 'sometimes|nullable|required_if:recurrence,repeat',
            'recurrence_day' => 'sometimes|nullable|required_if:recurrence,repeat',
         );
         $this->validate( $request , $rules);
         $events = new Events;
         $events->title = $request->get('title');
         $events->start_date = $request->get('start_date');
         $events->end_date = $request->get('end_date');
         $events->type = $request->get('recurrence');
         $events->save();
         $events = new Recurrences;
         $events->recurrence_First = $request->get('recurrence_First');
         $events->recurrence_weekday = $request->get('recurrence_weekday');
         $events->recurrence_month = $request->get('recurrence_month');
         $events->recurrence_every = $request->get('recurrence_every');
         $events->recurrence_day = $request->get('recurrence_day');
         $events->save();
         //return \View::make("event/list_event");
         return Redirect::to('/event/list');
   }
}
