<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Recurrences;
class Events extends Model
{
   protected $table = 'events';
   public function recurrence(){
    return $this->hasOne(Recurrences::class,'id','events_id');
   }
}
