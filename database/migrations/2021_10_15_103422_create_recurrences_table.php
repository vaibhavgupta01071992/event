<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecurrencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurrences', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('events_id')->unsigned()->index()->nullable();
            $table->foreign('events_id')->references('id')->on('events')->onDelete('cascade');
            $table->string('recurrence_First')->nullable();
            $table->string('recurrence_weekday')->nullable();
            $table->string('recurrence_month')->nullable();
            $table->string('recurrence_every')->nullable();
            $table->string('recurrence_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurrences');
    }
}
